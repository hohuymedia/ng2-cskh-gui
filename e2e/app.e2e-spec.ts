import { VinaphonePage } from './app.po';

describe('vinaphone App', function() {
  let page: VinaphonePage;

  beforeEach(() => {
    page = new VinaphonePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
