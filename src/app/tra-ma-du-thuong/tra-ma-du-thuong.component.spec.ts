/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TraMaDuThuongComponent } from './tra-ma-du-thuong.component';

describe('TraMaDuThuongComponent', () => {
  let component: TraMaDuThuongComponent;
  let fixture: ComponentFixture<TraMaDuThuongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraMaDuThuongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraMaDuThuongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
