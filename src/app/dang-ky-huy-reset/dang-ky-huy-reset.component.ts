import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { AppService } from '../app.service';
import { SsonService } from '../sson.service';

@Component({
  selector: 'app-dang-ky-huy-reset',
  templateUrl: './dang-ky-huy-reset.component.html',
  styleUrls: ['./dang-ky-huy-reset.component.css']
})
export class DangKyHuyResetComponent implements OnInit {
  data = [];
  pkgData = {};
  total = 0;
  pagination = {
    itemsPerPage: 20,
    currentPage: 0,
    totalItems: 0
  };

  constructor(public api:ApiService, public user: SsonService, private app:AppService) { }

  ngOnInit() {
  }

  search(): void {
    this.api.getAllPackages(+this.api.request.msisdn).then(res => {
      this.data = [];
      this.app.config.packageNames.map(e => {
        this.pkgData[e.code] = { "packagename": e.code };
      });

      res.json().packages.map(e => {
        this.pkgData[e.packagename] = e;
      });
      Object.keys(this.pkgData).map(key => {
        this.data.push(this.pkgData[key]);
      });
      console.log(this.data);
      this.total = res.json().totalpackage;
    }).catch(e => {
      this.app.noty(e.message || e.toString() || e);
    });
  }

  sendCommand(packagename: string, cmd:string): void{
    this.api.sendCommand(cmd, +this.api.request.msisdn, packagename).then(res =>{
      this.search();
    });
  }

}
