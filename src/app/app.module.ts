import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes} from '@angular/router';
// Third party libs
import { Ng2PaginationModule } from 'ng2-pagination';
import { DatepickerModule } from 'angular2-material-datepicker'
// App specific components
import { AppComponent } from './app.component';
import { TraCuuTbComponent } from './tra-cuu-tb/tra-cuu-tb.component';
import { LichSuDkHuyComponent } from './lich-su-dk-huy/lich-su-dk-huy.component';
import { LichSuTruCuocComponent } from './lich-su-tru-cuoc/lich-su-tru-cuoc.component';
import { LichSuSuDungComponent } from './lich-su-su-dung/lich-su-su-dung.component';
import { LichSuMoMtComponent } from './lich-su-mo-mt/lich-su-mo-mt.component';
import { DangKyHuyResetComponent } from './dang-ky-huy-reset/dang-ky-huy-reset.component';
import { ThongTinDichVuComponent } from './thong-tin-dich-vu/thong-tin-dich-vu.component';
import { AppService } from './app.service';
import { ApiService } from './api.service';
import { SsonService } from './sson.service';

// Define all routing modules for our apps
const routes: Routes = [
  { component:  TraCuuTbComponent, path: 'thue-bao', canActivate: [SsonService]},
  { component:  LichSuDkHuyComponent, path: 'tra-cuu/lich-su-dk-huy', canActivate: [SsonService]},
  { component:  LichSuTruCuocComponent, path: 'tra-cuu/lich-su-tru-cuoc', canActivate: [SsonService]},
  { component:  LichSuSuDungComponent, path: 'tra-cuu/lich-su-su-dung', canActivate: [SsonService]},
  { component:  LichSuMoMtComponent, path: 'tra-cuu/lich-su-mo-mt', canActivate: [SsonService]},
  { component:  DangKyHuyResetComponent, path: 'cai-dat/dang-ky-huy-reset', canActivate: [SsonService]},
  { component:  ThongTinDichVuComponent, path: 'thong-tin-dich-vu', canActivate: [SsonService]},

  // Default goes here... FIXME: Should be redirect to SSO page...
  { path: 'tra-cuu', redirectTo: '/tra-cuu/lich-su-dk-huy', pathMatch: 'full' , canActivate: [SsonService]},
  { path: 'cai-dat', redirectTo: '/cai-dat/dang-ky-huy-reset', pathMatch: 'full' , canActivate: [SsonService]},
  { path: '', redirectTo: '/thue-bao', pathMatch: 'full', canActivate: [SsonService]},
  { component:  ThongTinDichVuComponent, path: '**', canActivate: [SsonService]},
];


@NgModule({
  declarations: [
    AppComponent,
    TraCuuTbComponent,
    LichSuDkHuyComponent,
    LichSuTruCuocComponent,
    LichSuSuDungComponent,
    LichSuMoMtComponent,
    DangKyHuyResetComponent,
    ThongTinDichVuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RouterModule.forRoot(routes, { useHash: true }),
    Ng2PaginationModule,
    DatepickerModule
  ],
  providers: [
    AppService,
    ApiService,
    SsonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
