import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { AppService } from '../app.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-thong-tin-dich-vu',
  templateUrl: './thong-tin-dich-vu.component.html',
  styleUrls: ['./thong-tin-dich-vu.component.css']
})
export class ThongTinDichVuComponent implements OnInit {

  content = '';
  constructor(private http:Http, private app:AppService) { }

  ngOnInit() {
    this.http.get(this.app.config.infoUrl)
    .toPromise()
    .then( info => {
      this.content = info.text();
    });
  }
}
