import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-tra-cuu-tb',
  templateUrl: './tra-cuu-tb.component.html',
  styleUrls: ['./tra-cuu-tb.component.css']
})
export class TraCuuTbComponent implements OnInit {
  data = [];
  total = 0;
  pagination = {
    itemsPerPage: 20,
    currentPage: 0,
    totalItems: 0
  };

  constructor(public api:ApiService) { }

  ngOnInit() {
  }

  search(): void {
    this.api.getAllPackages(+this.api.request.msisdn).then(res => {
      this.data = res.json().packages.map(e => {
        e.servicename = this.api.getServiceName();
        e.expire_time = moment(e.expire_time, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss');
        e.last_time_renew = moment(e.last_time_renew, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss');
        e.last_time_retry = moment(e.last_time_retry, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss');
        e.last_time_subscribe = moment(e.last_time_subscribe, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss');
        e.last_time_unsubscribe = moment(e.last_time_unsubscribe, 'YYYYMMDDHHmmss').format('YYYY-MM-DD HH:mm:ss');
        return e;
      });
      this.total = res.json().totalpackage;
    });
  }
}
