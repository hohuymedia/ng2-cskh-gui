import { Component, OnInit } from '@angular/core';
import {Md5} from 'ts-md5/dist/md5';
import * as moment from 'moment';


@Component({
  selector: 'app-thong-tin-dich-vu',
  templateUrl: './thong-tin-dich-vu.component.html',
  styleUrls: ['./thong-tin-dich-vu.component.css']
})
export class ThongTinDichVuComponent implements OnInit {

  link = {
     cmd: 'REG',
     requestId: moment(new Date()).format('YYYYMMDDHHmmssSSSSSS'),
     returnUrl: 'http://nguhanhphongthuy.net',
     backUrl: 'http://nguhanhphongthuy.net',
     cp: 'CP_HOHUY',
     service: 'NGUHANHPHONGTHUY',
     securePass: 'CP_HOHUY@9999',
     requestDate: moment(new Date()).format('YYYYMMDDHHmmss'),
  };
  constructor() {
  }

  ngOnInit() {
  }
  resultLinks:string[] = [];


  generate(): void {
    this.resultLinks[0] = this.createLink('NGAY', 'WAP');
    this.resultLinks[1] = this.createLink('TUAN', 'WAP');
  }

  createLink(pkgName: string, channel:string):string {
    let secure = Md5.hashStr(this.link.requestId + this.link.returnUrl + this.link.backUrl + this.link.cp + this.link.service + pkgName + this.link.requestDate + channel + this.link.securePass);
    const vnpUrl = 'http://dk1.vinaphone.com.vn/?';

    return vnpUrl
    + 'requestid='+ this.link.requestId
    + '&returnurl='+ encodeURIComponent(this.link.returnUrl).toLowerCase()
    + '&backurl='+ encodeURIComponent(this.link.backUrl).toLowerCase()
    + '&cp='+ this.link.cp
    + '&service='+ this.link.service
    + '&package='+ pkgName
    + '&requestdatetime='+ this.link.requestDate
    + '&channel='+ channel
    + '&securecode='+ secure;
  }

}
