import { Injectable } from '@angular/core';
import { CanActivate }    from '@angular/router';
import { Http, Headers, Jsonp, URLSearchParams } from '@angular/http';
import { AppService } from './app.service';


import 'rxjs/add/operator/toPromise';

@Injectable()
export class SsonService implements CanActivate {

  private loggedIn = false;
  private user = { name: '', role: ''};

  constructor(private http: Http, private app: AppService, private jsonp: Jsonp) {
    this.loggedIn = !!localStorage.getItem('userInfo');
    if (this.loggedIn){
      this.user = JSON.parse(localStorage.getItem('userInfo'));
    } else {
      this.login();
    }
  }

  canActivate() {
    console.log('AuthGuard#canActivate called');
    return this.isLoggedIn();
  }

  login(): void {
    console.log("Got following info: ", this.app.config.sson);
    let params = new URLSearchParams();
    params.set('callback', 'JSONP_CALLBACK');
    this.jsonp.
      get(this.app.config.sson.ip +  '/SSO/SSOService.svc/user/RequestToken?callback=JSONP_CALLBACK', params)
      .toPromise()
      .then(ssodata => {
        var logonPage = this.app.config.sson.ip + '/SSO/Login.aspx?keyid=10001&URL=' + window.location.href;
        if (ssodata.json().Status == 'SUCCESS') {
            this.http.get(this.app.config.sson.authUrl + '?token=' +  ssodata.json().Token)
            .toPromise()
            .then(authResult => {
              this.user.name = authResult.json().username;
              this.user.role = authResult.json().role;
              if (this.user.name == ''){
                console.error("Invalid username!");
                this.app.noty("Sai tên đăng nhập");
              } else if (this.user.role == '') {
                var checkRoleUrl = this.app.config.sson.ip + '/Role/ServiceRole.svc/user/CheckRole?username=' + this.user.name;
                this.jsonp.request(checkRoleUrl)
                .toPromise()
                .then(resp => {
                  console.info("Get response from SSO page: ", resp);
                  this.user.role = resp.json().CheckRoleResult;
                })
                .catch(e => {
                  console.error("Cannot request to checkRoleUrl ", e);
                  this.app.noty("Không kết nối được tới máy chủ Đăng nhập!");
                });
              }
              localStorage.setItem('userInfo', JSON.stringify(this.user));
              this.loggedIn = true;
            }).catch(e => {
              console.log("Error handling auth URL response", e.message || e);
              this.app.noty("Không kết nối được tới máy chủ Đăng nhập!");
            });
        } else {
            window.location.href = logonPage;
        }
      })
      .catch((error) => {
        console.error("Error! Cannot connect to SSO login page! ", error.message || error);
        this.app.noty("Không kết nối được tới máy chủ Đăng nhập!");
      });
  }


  logout() {
    const logoutUrl = this.app.config.sson.ip + '/SSO/SSOService.svc/user/Logout?callback=JSONP_CALLBACK';
    let params = new URLSearchParams();
    params.set('callback', 'JSONP_CALLBACK');
    this.jsonp.
      get(logoutUrl, params)
      .toPromise()
      .then(res => {
        localStorage.removeItem('userInfo');
        this.loggedIn = false;
        window.location.href = this.app.config.sson.ip + '/SSO/login.aspx?keyid=10020&URL=' + window.location.href;
      }).catch(e => {
        console.error("Cannot access logout URL ", logoutUrl);
      });
  }

  isLoggedIn() {
    return this.loggedIn;
  }

  getUsername():string{
    return this.user.name;
  }

  getRole():string{
    if (this.user.role=="1") {
      return 'viewer';
    } else if (this.user.role == "2"){
      return 'admin';
    }
    return '';
  }

}
