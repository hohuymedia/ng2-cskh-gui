/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BuMaDuThuongComponent } from './bu-ma-du-thuong.component';

describe('BuMaDuThuongComponent', () => {
  let component: BuMaDuThuongComponent;
  let fixture: ComponentFixture<BuMaDuThuongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuMaDuThuongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuMaDuThuongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
