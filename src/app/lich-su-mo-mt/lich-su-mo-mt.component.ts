import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import * as moment from 'moment';

@Component({
  selector: 'app-lich-su-mo-mt',
  templateUrl: './lich-su-mo-mt.component.html',
  styleUrls: ['./lich-su-mo-mt.component.css']
})
export class LichSuMoMtComponent implements OnInit {
  data = [];
  total = 0;
  pagination = {
    itemsPerPage: 20,
    currentPage: 0,
    totalItems: 0
  };

  constructor(public api:ApiService) { }

  ngOnInit() {
  }

  search(): void {
    this.api.getMtHistory(+this.api.request.msisdn, this.api.request.fromdate, this.api.request.todate, this.pagination.itemsPerPage, this.pagination.currentPage -1)
    .then(res => {
      this.data = res.json().data.map(row => {
        if (row.mo_info == null) return row;
        var group = row.mo_info.match(/MO\|([^|]+)\|/ig);
        if (group == null) return row;
        if ((row.mo_type == 1) && (group.length >= 1)){
          row.mo = group[0].split('|')[1];
        } else if (group.length > 1) {
          row.mo = group[1].split('|')[1];
        }
        return row;
      })
      this.pagination.totalItems = res.json().totalpage * this.pagination.itemsPerPage;
    });
  }

  pageChanged($event: number):void {
    this.pagination.currentPage = $event;
    this.search();
  }
}
