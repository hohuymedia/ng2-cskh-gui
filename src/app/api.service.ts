import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { AppService } from './app.service';
import * as moment from 'moment';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ApiService {
  private headers = new Headers({'Content-Type': 'application/json'});
  request: any = {};
  constructor(private http:Http, private app:AppService) { }

  private newRequest():any{
    this.request = {
      requestid:moment(new Date()).format('YYYYMMDDHHmmss'),
      application: this.app.config.api.application,
      channel: this.app.config.api.channel,
      username: this.app.config.api.username,
      userip: this.app.config.api.userip
    };
    return this.request;
  }

  /** Search all packages for a subscriber **/
  getAllPackages(msisdn:number): Promise<any>{
    let req = Object.assign(this.newRequest(),{
      "msisdn": this.mcc(msisdn)
    });
    return this.http.post(this.app.config.api.subInfo, JSON.stringify(req), {headers: this.headers}).toPromise()
    .catch(error => {
      this.app.noty("Không lấy được dữ liệu!");
    });;
  }

  /** Check one package for subscriber **/
  getPackage(msisdn: number, packagename: string): Promise<any>{
    let req = Object.assign(this.newRequest(),{
      "msisdn": this.mcc(msisdn),
      "packagename": packagename
    });
    return this.http.post(this.app.config.api.subPkgInfo, JSON.stringify(req), {headers: this.headers}).toPromise()
    .catch(error => {
      this.app.noty("Không lấy được dữ liệu!");
    });;
  }


  /** get Transaction INfo **/
  getSubscriptionHistory(msisdn:number, fromDate:string, toDate:string, pageSize:number, pageIndex:number): Promise<any>{
    let req = Object.assign(this.newRequest(),{
      "msisdn": this.mcc(msisdn),
      "fromdate": fromDate,
      "todate": toDate,
      "pagesize": pageSize,
      "pageindex": pageIndex,
    });
   return this.http.post(this.app.config.api.subHistory, JSON.stringify(req), {headers: this.headers}).toPromise()
   .catch(error => {
     this.app.noty("Không lấy được dữ liệu!");
   });;
  }

  /** get Transaction INfo **/
  getUsageHistory(msisdn:number, fromDate:string, toDate:string, pageSize:number, pageIndex:number): Promise<any>{
    let req = Object.assign(this.newRequest(),{
     "msisdn": this.mcc(msisdn),
     "fromdate": fromDate,
     "todate": toDate,
     "pagesize": pageSize,
     "pageindex": pageIndex,
   });
   return this.http.post(this.app.config.api.usageHistory, JSON.stringify(req), {headers: this.headers}).toPromise()
   .catch(error => {
     this.app.noty("Không lấy được dữ liệu!");
   });;
  }

  /** get SMS info **/
  getMtHistory(msisdn:number, fromDate:string, toDate:string, pageSize:number, pageIndex:number): Promise<any>{
    let req = Object.assign(this.newRequest(),{
     "msisdn": this.mcc(msisdn),
     "fromdate": fromDate,
     "todate": toDate,
     "pagesize": pageSize,
     "pageindex": pageIndex,
   });
   return this.http.post(this.app.config.api.mtHistory, JSON.stringify(req), {headers: this.headers}).toPromise()
   .catch(error => {
     this.app.noty("Không lấy được dữ liệu!");
   });;
  }

  /** Retrieve charging history information from API server **/
  getChargingHistory(msisdn:number, fromDate:string, toDate:string, pageSize:number, pageIndex:number): Promise<any>{
    let req = Object.assign(this.newRequest(),{
     "msisdn": this.mcc(msisdn),
     "fromdate": fromDate,
     "todate": toDate,
     "pagesize": pageSize,
     "pageindex": pageIndex,
   });
   return this.http.post(this.app.config.api.chargeHistory, JSON.stringify(req), {headers: this.headers}).toPromise()
   .catch(error => {
     this.app.noty("Không lấy được dữ liệu!");
   });
  }

  /** sending command to server to sub / unsub **/
  sendCommand(cmd: string, msisdn: number, packagename: string): Promise<any>{
    let req = Object.assign(this.newRequest(), {
     "msisdn": this.mcc(msisdn),
     "command": cmd,
     "packagename": packagename,
   });
   return this.http.post(this.app.config.api.sendCommand, JSON.stringify(req), {headers: this.headers})
   .toPromise()
   .then(res => {
     this.app.noty("Gửi lệnh thành công! Kết quả: " + res.json().error_desc + " - " + res.json().extra_information);
     return res;
   })
   .catch(error => {
     this.app.noty("Không thực hiện được thao tác!");
   });
  }

  /** Helper function to convert 0912xxx / 912xxx to 84912xxx */
  mcc(isdn:any): number {
    var msisdn:number = +(isdn.toString());
    if (msisdn.toString().substring(0, 2) != '84') {
      msisdn = +('84' + msisdn.toString());
    }
    if (!msisdn.toString().match(/^84[0-9]{9,10}$/)){
      msisdn = +(msisdn.toString().substr(0,12));
    }
    return msisdn;
  }


  getServiceName(){
    return this.app.config.serviceName;
  }


}
