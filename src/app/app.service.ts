import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class AppService {

  config={
    "appName": "GUI CSKH",
    "infoUrl": window.location.origin + window.location.pathname + "config/info.html",
    "contact": {
      "name": "",
      "email": "",
      "phone": ""
    },
    "serviceName": "",
    "packageNames": [],
    "sson": {
      "ip": "http://10.211.0.250:8080/",
      "authUrl": "/api/sson"
    },
    "api": {
      "application": "CSKH",
      "channel": "CSKH",
      "username": "",
      "userip": "",
      "subInfo": "/api/sub-info-all",
      "subPkgInfo": "/api/sub-info",
      "subHistory": "/api/subscription-history",
      "usageHistory": "/api/usage-history",
      "mtHistory": "/api/mt-history",
      "chargeHistory": "/api/charging-history",
      "sendCommand": "/api/send-command"
    }
  };
  constructor(private http:Http){}
  load(): Promise<any> {
    return this.http.get(window.location.origin + window.location.pathname + 'config/app.json')
    .toPromise()
    .then(data => {
      this.config = Object.assign(this.config, data.json());
    })
    .catch(e => {
      this.noty("Không tìm được file cấu hình dịch vụ!");
    });
  }
  /** Display modal dialog **/
  modal = {
    show: false,
    title: 'Thông báo',
    msg: '',
    close(): void{
      this.show = false;
    }
  }
  noty(msg: string){
    this.modal.msg = msg;
    this.modal.show = true;
  }

}
