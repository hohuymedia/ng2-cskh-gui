import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-lich-su-su-dung',
  templateUrl: './lich-su-su-dung.component.html',
  styleUrls: ['./lich-su-su-dung.component.css']
})
export class LichSuSuDungComponent implements OnInit {
  data = [];
  pagination = {
    itemsPerPage: 20,
    currentPage: 0,
    totalItems: 0
  };
  constructor(public api:ApiService) { }

  ngOnInit() {
  }

  search():void {
    this.api.getUsageHistory(+this.api.request.msisdn, this.api.request.fromdate, this.api.request.todate, this.pagination.itemsPerPage, this.pagination.currentPage -1)
      .then(response => {
        this.data = response.json().data;
        this.pagination.totalItems = response.json().totalpage * this.pagination.itemsPerPage;
      })
      .catch(e => {console.error("Error request API")});
  }

  pageChanged($event: number):void {
    this.pagination.currentPage = $event;
    this.search();
  }

}
