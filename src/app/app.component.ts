import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SsonService } from './sson.service';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(public user:SsonService, public app: AppService, private pageTitle: Title){ }
  ngOnInit(): void {
    this.app.load().then(e => {
      console.log("Application configuration loaded and can be access via app.config", this.app.config);
      this.pageTitle.setTitle("GUI CSKH | " + this.app.config.serviceName + "- Hồ Huy Media");
      if (!this.user.isLoggedIn()){
        this.user.login();
      }
    })
    .catch(e => {
      console.error("Missing mandatory config/app.json file ... stop processing");
    });
  }
}
