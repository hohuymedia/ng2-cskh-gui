/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SsonService } from './sson.service';

describe('Service: Sson', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SsonService]
    });
  });

  it('should ...', inject([SsonService], (service: SsonService) => {
    expect(service).toBeTruthy();
  }));
});
