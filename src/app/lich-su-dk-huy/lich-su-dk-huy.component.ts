import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';


@Component({
  selector: 'app-lich-su-dk-huy',
  templateUrl: './lich-su-dk-huy.component.html',
  styleUrls: ['./lich-su-dk-huy.component.css']
})
export class LichSuDkHuyComponent implements OnInit {
  data = [];
  pagination = {
    itemsPerPage: 20,
    currentPage: 0,
    totalItems: 0
  };
  constructor(public api:ApiService) { }

  ngOnInit() {
  }

  search():void {
    this.api.getSubscriptionHistory(+this.api.request.msisdn, this.api.request.fromdate, this.api.request.todate, this.pagination.itemsPerPage, this.pagination.currentPage -1)
      .then(response => {
        this.data = response.json().data;
        this.pagination.totalItems = response.json().totalpage * this.pagination.itemsPerPage;
      })
      .catch(e => {console.error("Error request API")});
  }

  pageChanged($event: number):void {
    this.pagination.currentPage = $event;
    this.search();
  }

}
