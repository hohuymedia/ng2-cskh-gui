import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-lich-su-tru-cuoc',
  templateUrl: './lich-su-tru-cuoc.component.html',
  styleUrls: ['./lich-su-tru-cuoc.component.css']
})
export class LichSuTruCuocComponent implements OnInit {
  data = []; total = 0;
  pagination = {
    itemsPerPage: 20,
    currentPage: 0,
    totalItems: 0
  };
  constructor(public api:ApiService, public app:AppService) { }

  ngOnInit() {
  }

  search():void {
    this.api.getChargingHistory(this.api.request.msisdn, this.api.request.fromdate, this.api.request.todate, this.pagination.itemsPerPage, this.pagination.currentPage -1)
      .then(response => {
        this.data = response.json().data;
        this.pagination.totalItems = response.json().totalpage * this.pagination.itemsPerPage;
        this.data.map(e => {
          this.total += +e.price;
        });
      })
      .catch(e => {console.error("Error request API")});
  }

  pageChanged($event: number):void {
    this.pagination.currentPage = $event;
    this.search();
  }

}
