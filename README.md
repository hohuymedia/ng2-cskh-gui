# Vinaphone

## Cách dùng

- Copy thư mục `/dist` lên web server.
- Copy thư mục `/config` vào trong thư mục `/dist`
- Sửa lại 2 file `config/app.json` và `config/info.html`.
- Done.

__File `app.json`__:

~~~json
{
  "appName": "GUI CSKH - TÊN DỊCH VỤ",
  "contact": {
    "name": "ĐẦU MỐI CSKH",
    "email": "CSKH_EMAIL",
    "phone": "CSKH_PHONE"
  },
  "packageNames": [
    { "code": "MÃ GÓI NGÀY", "name": "TÊN GÓI CƯỚC" },
    { "code": "MÃ GÓI TUẦN", "name": "TÊN GÓI CƯỚC" },
  ],
  "sson": {
    "ip": "http://10.211.0.250:8080/",
    "authUrl": "http://domain.com/api/user"
  },
  "api": {
    "subInfo": "http://domain.com/api/sub-info-all",
    "subPkgInfo": "http://domain.com/api/sub-info",
    "subHistory": "http://domain.com/api/subscription-history",
    "usageHistory": "http://domain.com/api/usage-history",
    "mtHistory": "http://domain.com/api/mt-history",
    "chargeHistory": "http://domain.com/api/charging-history",
    "sendCommand": "http://domain.com/api/send-command"
  }
}
~~~

## Cách build project

Dùng docker image `nguyendinhtrung141/ng2-cli` để tạo môi trường.

~~~
# Download project về máy
git clone git@bitbucket.org:hohuymedia/vinaphone

# Chuyển vào thư mục code
cd vinaphone

# Chạy docker container vinaphone_nodejs_1 để tạo môi trường
docker-compose up -d

# Chui vào container
docker exec -it vinaphone_nodejs_1 ash
~~~

Chạy các lệnh sau trong container

~~~
cd /src
npm install -g angular-cli
npm install
~~~

### Development

Để mở server phát triển dự án

~~~
ng serve --host 0.0.0.0 --port 3000
~~~

Truy cập http://localhost:3000 để mở project

### Build project

~~~
# Để tạo project chạy tại thư mục gốc của web server, vd: http://some.domain/
ng build -prod

# Để tạo project chạy tại sub-folder của web server, vd: http://some.doman/some-path
ng build -prod -bh /some-path/
~~~

## Mô tả các chức năng chính

__`api.service.ts`__: Cung cấp các hàm giao tiếp API với backend qua XHR / JSONP.
__`sson.service.ts`__: Cung cấp chức năng Single sign-on với SSO gateway của Vinaphone.

### Các bản tin request và response giữa frontend - backend

__`api.getAllPackages(msisdn)`: Danh sách gói cước thuê bao đang dùng__

~~~
POST -X POST -data @data.json http://domain.com/api/sub-info-all
{"requestid":"20161107223834","msisdn":84949380145,"application":"VASGW","channel":"CSKH","username":"","userip":""}

{
  "errorid":0, "errordesc":"Thanh cong",
  "totalpackage":6,
  "packages":[
    {"packagename":"KQMB","status":"0","last_time_subscribe":"20161107164735","last_time_unsubscribe":"20161107165243","last_time_renew":"20161107164735","last_time_retry":"20161107164735","expire_time":"20161107165243"},
    ...
    {"packagename":"TKMN","status":"0","last_time_subscribe":"20161102145109","last_time_unsubscribe":"20161104152827","last_time_renew":"20161102145109","last_time_retry":"20161102145109","expire_time":"20161104152827"},
  ]
}
~~~

__`api.getChargingHistory(msisdn)`: Lấy các giao dịch trừ cước__

~~~
POST -X POST -data @data.json http://domain.com/api/charging-history

{"requestid":"20161107230809","msisdn":84949380145,"fromdate":"","todate":"","pagesize":20,"pageindex":-1,"application":"CSKH","channel":"CSKH","username":"trungnd","userip":"172.17.0.1"}

{
    "errorid": 0,
    "errordesc": "Thành công",
    "totalpage": 3,
    "data": [{
        "datetime": "2016-11-07 16:52:43",
        "eventname": "UNREG",
        "packagename": "KQMB",
        "price": "0",
        "application": "",
        "channel": "",
        "username": "",
        "userip": ""
    }, {
        "datetime": "2016-11-07 16:47:36",
        "eventname": "REG",
        "packagename": "KQMB",
        "price": "2000",
        "application": "",
        "channel": "",
        "username": "",
        "userip": ""
    }]
}   

~~~

__`api.getSubscriptionHistory(msisdn:number, fromDate:string, toDate:string, pageSize:number, pageIndex:number)`: Lấy các giao dịch đăng ký / hủy__

~~~
POST -X POST -data @data.json http://domain.com/api/subscription-history

{"requestid":"20161107230809","msisdn":84949380145,"fromdate":"","todate":"","pagesize":20,"pageindex":-1,"application":"CSKH","channel":"CSKH","username":"trungnd","userip":"172.17.0.1"}

{
    "errorid": 0,
    "errordesc": "Thành công",
    "totalpage": 3,
    "data": [{
        "datetime": "2016-11-07 16:52:43",
        "eventname": "UNREG",
        "packagename": "KQMB",
        "price": "0",
        "status": "1",
        "application": "",
        "channel": "",
        "username": "",
        "userip": ""
    },
    ....
    , {
        "datetime": "2016-11-02 14:42:33",
        "eventname": "UNREG",
        "packagename": "KQMN",
        "price": "0",
        "status": "1",
        "application": "",
        "channel": "",
        "username": "",
        "userip": ""
    }]
}
~~~

__`api.getUsageHistory(msisdn:number, fromDate:string, toDate:string, pageSize:number, pageIndex:number)`: Lấy lịch sử sử dụng__

~~~
POST -X POST -data @data.json http://domain.com/api/usage-history

{"requestid":"20161107230809","msisdn":84949380145,"fromdate":"","todate":"","pagesize":20,"pageindex":-1,"application":"CSKH","channel":"CSKH","username":"trungnd","userip":"172.17.0.1"}

{
    "errorid": 0,
    "errordesc": "Thành công",
    "totalpage": 2,
    "data": [{
        "packagename": "KQMB",
        "status": "0",
        "last_time_subscribe": "20161107164735",
        "last_time_unsubscribe": "20161107165243",
        "last_time_renew": "20161107164735",
        "last_time_retry": "20161107164735",
        "expire_time": "20161107165243"
    },
    ...
    {
        "packagename": "KQMB",
        "status": "0",
        "last_time_subscribe": "20161107153726",
        "last_time_unsubscribe": "20161107163121",
        "last_time_renew": "20161107153726",
        "last_time_retry": "20161107153726",
        "expire_time": "20161107163121"
    }]
}
~~~

__`api.getMtHistory(msisdn:number, fromDate:string, toDate:string, pageSize:number, pageIndex:number)`: Lấy lịch sử SMS MT__

~~~
POST -X POST -data @data.json http://domain.com/api/mt-history

{"requestid":"20161107230809","msisdn":84949380145,"fromdate":"","todate":"","pagesize":20,"pageindex":-1,"application":"CSKH","channel":"CSKH","username":"trungnd","userip":"172.17.0.1"}

{
    "errorid": 0,
    "errordesc": "Thành công",
    "totalpage": 0,
    "data": [{
        "datetime": "2016-11-07 16:53:28",
        "shortcode": "9855",
        "msisdn": "84949380145",
        "message": "Quy khach chua dang ky goi KQXS Mien Bac cua dich vu XO SO PHAT LOC. Soan  DK MB gui 9855 de su dung dich vu. Chi tiet truy cap www.domain.com hoac goi 19006821. Tran trong cam on!"
    },
    ...
    {
        "datetime": "2016-11-07 16:52:43",
        "shortcode": "9855",
        "msisdn": "84949380145",
        "message": "Quy khach da huy thanh cong goi KQXS Mien Bac cua dich vu XO SO PHAT LOC. De dang ky lai, soan DK MB gui 9855. Chi tiet truy cap www.domain.com hoac goi 19006821. Tran trong cam on!"
    }]
}
~~~


__`api.sendCommand(cmd: string, msisdn: number, packagename: string)`: Gửi lệnh đăng ký / hủy lên server để đẩy sang VASPRO__

~~~
POST -X POST -data @data.json http://domain.com/api/send-command
{"requestid":"20161107231321","application":"CSKH","channel":"CSKH","username":"trungnd","userip":"172.17.0.1",
"msisdn":84949380145,"command":"reg","packagename":"KQMB"}

# Mã lỗi do VASPro trả về
{"@attributes":{"name":"subscribe"},"requestid":"20161107231453","error":"-8","error_desc":"reject","extra_information":"1|over quota"}
~~~
